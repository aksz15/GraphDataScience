import spacy
# load English model
nlp = spacy.load("en_core_web_sm")
text = "Leonardo DiCaprio is born in Los Angeles."
# analyze text
document = nlp(text)
# print entities
for ent in document.ents:
    print(ent.text, ":", ent.label_)
