import spacy
# load English model
nlp = spacy.load("en_core_web_sm")
text = "Leonardo DiCaprio is born in Los Angeles."
# analyze text
document = nlp(text)
# print cypher
for ent in document.ents:
    q = f"MATCH (n:{ent.label_} {{name: '{ent.text}' }})"
    print(q)
for token in document:
    if token.pos_ == "VERB":
        print(f"[:{token.text.upper()}]")
